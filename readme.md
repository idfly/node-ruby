Node with ruby
==============

General purpose node image by idfly.ru.

Contains:

  * packages: ruby
  * gems: bundler
  * path extensions in order to run npm commands
